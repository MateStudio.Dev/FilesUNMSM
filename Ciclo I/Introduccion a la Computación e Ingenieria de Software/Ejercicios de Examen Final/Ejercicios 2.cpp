#include<iostream>
#include<cstdlib>
using namespace std;
int main()
{
    int n;
    float monto=0,igv;
    cout<<"Ingrese el numero de prendas: ";cin>>n;
    int r[n],opc,c=1;//opc:tipo de prenda, c:cuenta prendas iguales
    for(int i=0;i<n;i++)
    {
        cout<<"\ningrese el tipo de prenda:\n";
        cout<<"(0)Camisa\tS/. 1.50\n(1)Saco\t\tS/. 2.00\n(2)Chaleco\tS/. 2.50\n(3)Pantalon\tS/. 3.00\n(4)Otros\tS/. 3.50\n";
        cin>>opc;
        if(opc>=0 && opc<=3)
            r[i]=opc;
        else
            r[i]=4;//prendas no clasificables
        if(i>0 && r[i]!=4 && r[i]==r[i-1])
            c++;
        switch(opc)
        {
            case 0:
                monto+=1.5;break;
            case 1:
                monto+=2.0;break;
            case 2:
                monto+=2.5;break;
            case 3:
                monto+=3.0;break;
            case 4:
                monto+=3.5;break;
        }
    }
    int d;
    switch(n)
    {
        case 1:
            d=0;break;
        case 2:
            d=10;break;
        case 3:
            d=15;break;
        case 4:
            d=20;break;
        default:
            d=25;
    }
    if(c==n)
        d+=5;
    cout<<"Ud. obtuvo un descuento del "<<d<<"% sobre S/."<<monto<<endl;
    monto-=(monto*d/100);
    cout<<"Monto a pagar: "<<monto<<endl;
    igv=monto*0.19;
    cout<<"IGV: "<<igv<<endl;
    cout<<"TOTAL: "<<monto+igv<<endl;
    system("PAUSE");
    return 0;
}

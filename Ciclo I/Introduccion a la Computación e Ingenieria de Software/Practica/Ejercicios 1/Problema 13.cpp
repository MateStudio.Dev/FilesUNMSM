#include <iostream>
#include <cstdlib>
void leer(int x[][100],int n);
void TriangularSuperior(int x[][100],int y[][100],int n);
using namespace std;
int main()
{
    int n;
    cout<<"\nIngrese el tamano de la matriz cuadrada: ";
    cin>>n;
    int a[n][100],b[n][100];
    cout<<endl;
    leer(a,n);
    
    TriangularSuperior(a,b,n);
    
    system("PAUSE");
    return 0;
}
void leer(int x[][100],int n)
{
    for(int i=0;i<n;i++)
    {
        cout<<"Ingrese los datos de la "<<i+1<<" fila: "<<endl;
        for(int j=0;j<n;j++)
        {
                cin>>x[i][j];
        }
    cout<<endl;
    }
    cout<<"La Matriz Original es:"<<endl;
    for(int i=0;i<n;i++)
     {
         for(int j=0;j<n;j++)
         {
                cout<<x[i][j]<<" ";
         }
         cout<<endl;
     }
     cout<<endl;
} 
void TriangularSuperior(int x[][100],int y[][100],int n)
{
     for(int i=0;i<n;i++)
     {
         for(int j=0;j<n;j++)
         {
                 y[i][j]=x[i][j];
                 if(i>j)
                 {
                        y[i][j]=0;
                 }
         }
     }
     cout<<"La Matriz Triangular Superior es:"<<endl;
     for (int i=0;i<n;i++)
     {
         for(int j=0;j<n;j++)
         {
                 cout<<y[i][j]<<" ";
         }
         cout<<endl;
     }
     cout<<endl;   
}

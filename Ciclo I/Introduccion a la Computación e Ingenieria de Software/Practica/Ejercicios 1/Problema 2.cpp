#include <iostream>
#include <cstdlib>
void leer(int x[],int num);
void ordenar(int x[],int num);
int aprobados(int x[],int num);
using namespace std;
int main()
{
    int notas[100],n;
    cout<<"Ingrese cuantas notas desea ordenar: ";
    cin>>n;
    leer(notas,n);
    ordenar(notas,n);
    cout<<"La cantidad de aprobados es: "<<aprobados(notas,n)<<endl;
    system("PAUSE");
    return 0;
}
void leer(int x[],int num)
{
     for (int i=0;i<num;i++)
     {
         cout<<"Ingrese la nota "<<i+1<<" : ";
         cin>>x[i];
     }
}
void ordenar(int x[],int num)
{
     int mayor;
     for (int i=0;i<num;i++)
     {
         for(int j=i+1;j<num;j++)
         {
                if (x[i]>x[j])
                {
                              mayor=x[i];
                              x[i]=x[j];
                              x[j]=mayor;
                }
         }
     }
     cout<<"Las notas ordenadas son: ";
     for (int i=0;i<num;i++)
     {
         cout<<x[i]<<" ";
     }
     cout<<endl;
     cout<<"La  menor nota es: "<<x[0]<<endl;
     cout<<"La  mayor nota es: "<<x[num-1]<<endl;
}
int aprobados(int x[],int num)
{
    int c=0;
    for (int i=0;i<num;i++)
    {
        if (x[i]>=11)
        {
              c++;
        }
    }
    return c;
}  
     

#include<iostream>
#include<cstdlib>
#include<math.h>
using namespace std;
void coordenadas(float a[],float b[],int);
void forma_polar(float a[],float b[],int);
int main ()
{    
    int n;
    cout<<"\nIngrese la Cantidad de Puntos a registrar: ";
    cin>>n;
    float Coord_X[n],Coord_Y[n];
    coordenadas(Coord_X,Coord_Y,n);
    cout<<"\nSe tienen las siguientes formulas:"<<endl;
    cout<<"r = [(x)^2 + (y)^2]^(1/2)"<<endl;
    cout<<"tetha = tan(y/x)^(-1)"<<endl;
    forma_polar(Coord_X,Coord_Y,n);
    cout<<endl;
    system("PAUSE");
    return 0;
}

void coordenadas(float a[],float b[],int c)
{
  for(int i=0;i<c;i++)
  {
    cout<<"\nIngrese las coordenadas x , y del punto "<<i+1<<": "<<endl;
    cin>>a[i]>>b[i];
  }
}  
void forma_polar(float a[],float b[],int c)
{
  float r[c];
  float tetha[c];
  for(int i=0;i<c;i++)
  {
     r[i]=sqrt(a[i]*a[i]+b[i]*b[i]);
     tetha[i]=atan(b[i]/a[i]);
  }
  for(int i=0;i<c;i++)
  {
     cout<<"\nDel Punto "<<i+1<<" se pudo calcular: "<<endl;
     cout<<"r = "<<r[i]<<endl;
     cout<<"tetha = "<<tetha[i]<<endl;
  }
}  

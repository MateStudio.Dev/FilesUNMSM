#include <iostream>
#include <cstdlib>
using namespace std;
void pro(int);
int main ()
{
    int n;
    cout<<"Ingrese un numero:";cin>>n;
    while ( cin.fail() )
    { 
        cin.clear();  // hay que limpiar las banderas del cin,  
        cin.ignore( 2000, '\n' ); // vacia el buffer de la entrada
        
        // solicita la entrada de un entero
        cout << "\nValor no aceptable\n" ;
        cout << "Por favor digite un entero: ";
        cin >> n;
    }
    if(n>=0&&n<=9999)
    {
        pro(n);
    }
    system("PAUSE");
    return 0;
}
void pro(int q)
{
    int i=1;
    while(q>0)
    {
        switch(i)
        {
            case 1:
                 cout<<"El numero de unidades es\t"<<q%10<<endl;
                 break;
            case 2:
                 cout<<"El numero de decenas es\t"<<q%10<<endl;
                 break;
            case 3:
                 cout<<"El numero de centenas es\t"<<q%10<<endl;
                 break;
            case 4:
                 cout<<"El numero de millares es\t4"<<q%10<<endl;
                 break;
        }
        i++;
        q/=10;
    }
}

#include <iostream>
#include <cstdlib>
#include <cstring>
using namespace std;


int main(){
    char cad1[5]; //cadena de instruccion generada por el usuario
    char cad2[3]=""; //almacena la primera parte de la cadena
    char cad3[3]=""; //almacena la segunda parte de la cadena
    int s1,s2; //sumandos
    do{
        system("cls");
        cout<<"Sumar [XX,YY]: ";
        cin>>cad1;      
        if(strlen(cad1)!= 5){ // Si la longitud de cad1 no es de 5 caracteres
            cout<<"\nError de instruccion\n";
            exit(1); //aborta el programa, (1) indica terminacion normal
        }
        else if (cad1[2]!=','){ //Si el tercer caracter no es coma(,)
            cout<<"\nError de instruccion\n";
            exit(1); //aborta el programa, (1) indica terminacion normal
        }
        else {
            strncpy(cad2,cad1,2); //copia los primeros 2 caracteres de cad1
            strncpy(cad3,cad1+3,2); //copia los ultimos 2 caracteres de cad1
            s1=atoi(cad2); //se convierte cad2 a entero y se almacena en s1
            s2=atoi(cad3);
            //calculo de la suma de los 2 enteros
            cout<<"\nSUMA = "<<s1+s2;
            cout<<"\nDigite [sumar] para efectuar otra suma";
            cout<<"\n[salir] para abandonar el programa: ";
        }
        do{
            cin>>cad1;
        }while((strcmp(cad1,"sumar")!=0) && (strcmp(cad1,"salir")!=0));
        //strcmp compara cad1 con la palabra "sumar" si cad1 es igual a "sumar",
        // strcmp devuelve el valor de 0 y el lazo se repite mientras cad1 
        //sea igual a sumar
    }while(strcmp(cad1,"sumar")==0);
    
    system("PAUSE");
    return (0);
}



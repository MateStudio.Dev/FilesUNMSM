/*Desarrollar un programa que imprima todos los n�meros naturales que hay desde la
unidad hasta un n�mero que introducimos por teclado.*/
#include <iostream>
#include <cstdlib>
using namespace std;
int main()
{ 
    int a,i;
    cout << "Ingrese un numero natural: ";
    cin >> a;
    cout << endl;
    if(a>=0)
    {
      for (i=1;i<=a;i++)
      {
        cout << i << endl;
      }
    }
    else
    {
      cout << " * El numero tiene que ser mayor o igual a cero" << endl;     
    }
    cout << endl;
    system("PAUSE");
    return 0;
}
    

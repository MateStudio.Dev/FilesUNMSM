/*El enunciado de este ejercicio esta incorrecto*/

#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;
int main()
{
    int i,j; //variables enteras que seran usadas en el for
    float fact,e,x; //variables reales para las operaciones
    i=1; //variable que servira de limite para el for tambien ira incrementando las potencia
    e=1; //punto inicial de la serie
    cout << "Ejerccio 5.11" << endl;
    cout << endl;
    cout << "Ingrese un valor de x para la formula e^x: ";
    cin >> x;
    cout << endl;
    do //use un do while por que antes de evaluar la condicion del while necesitaba un factorial que se halla despues de correr una vez el do while
    {
        fact=1; // el factorial se regresara a 1 para que cambie junto a las potencias
        for(j=1;j<=i;j++) //for usado unicamente para hallar el factorial de cada caso
        {
                       fact*=j;
        }
        //una vez hallado el factorial se usara para cada operacion
        e=e+(pow(x,float(i)))/fact; //se convirtio el i a real ya que el pow no admite enteros
        i=i+1; //se incrementa en 1 la potencia
    }
    //condicion indicada en el ejercicio 
    while (((pow(x,float(i)))/fact)>pow(float(10),float(-4))); //se tuvo que usar valores reales ya que no funciono con los enteros
    cout << "La potencia e^x es: " << e << endl;
    cout << endl;
    system("PAUSE");
    return 0;
}  

#include<iostream>
#include<cstdlib>
using namespace std;
int main()
{
    //declaracion de variables
    int a,b,n,i,t;
    a=1; //primer termino 1 
    b=2; //segundo termino 2
    cout << "Ejerccio 5.12" << endl;
    cout << endl;
    cout << "Ingrese la posicion de la serie fibonacci que quiere hallar: ";
    cin >> t;
    cout << endl;
    //como lo indica el problema se usaran valores mayores o iguales a 3
    if(t>=3)
    {
            for(i=2;i<t;i++)
            {
                n=a+b; //variable que es la suma de dos anteriores
                a=b; //el primer termino toma el valor del segundo
                b=n; //el segundo termino toma el valor de la suma          
            }
            cout << "  * El termino del posicion " << t << " es: " << n << endl;  
    }
    else
    {
        cout << "  * Debe ingresar valores mayores o iguales a 3" << endl;
    }      
    cout << endl;        
    system("PAUSE");
    return 0;
}   

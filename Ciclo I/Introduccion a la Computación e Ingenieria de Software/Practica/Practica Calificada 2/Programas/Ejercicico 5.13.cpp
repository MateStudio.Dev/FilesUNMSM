#include<iostream>
#include<cstdlib>
using namespace std;
int main()
{
    //variables
    int e,c,i,m;
    m=0; //contador de gente mayor
    cout << "Ejerccio 5.13" << endl;
    cout << endl;
    cout << "Ingrese el numero de empleados: ";
    cin >> c;
    cout << endl;
    while(c<0) //validacion para datos negativos
    {
          cout << "  * Debe ingresar un valor mayor a 0: ";
          cin >> c;
          cout << endl;
    }
    cout<<endl; 
    for(i=1;i<=c;i++) //lectura de edades
    {
        cout << "  * Ingrese la edad del empleado " << i << ": ";
        cin >> e;
        cout << endl;
        while(e<18) //validacion para las edades de los empleados
        {
          cout << "  * Los empleados deben cumplir la mayoria de edad: ";
          cin >> e;
          cout << endl;
        } 
        if(e>65)
        {
                 m=m+1; //contador de gente mayor
        }
    }
    cout << "El numero de empleados mayores de 65 anos es: " << m << endl;
    cout<<endl;
    system("PAUSE");
    return 0;
}

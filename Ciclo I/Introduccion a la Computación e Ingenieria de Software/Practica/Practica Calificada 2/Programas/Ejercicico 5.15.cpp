#include<iostream>
#include<cstdlib>
using namespace std;
int main()
{
    //variables
    int i,s,sa,sm,sb;
    sa=sm=sb=0; //contadores
    cout << "Ejerccio 5.15" << endl;
    cout << endl;
    for(i=1;i<=50;i++) //condicion del problema
    {
                      cout <<"Ingrese el sueldo del empleado "<< i <<": ";
                      cin >> s;
                      cout << endl;
                      while (s<0) //validacion del sueldo
                      {
                            cout <<"  * El sueldo debe ser positivo: ";
                            cin >> s;
                            cout << endl;
                      }
                      //se evalua el rango en donde se encuentra el sueldo
                      if (s>300000)
                      {
                                   sa=sa+1;
                      }
                      if (s<=300000 && s>=100000)
                      {
                                    sm=sm+1;
                      }
                      if (s<100000)
                      {
                                    sb=sb+1;
                      }
    }
    //muestra de resultados
    cout <<"Los empleados con salarios altos (ganan mas de 300000 pesetas) son: "<<sa<<endl;
    cout <<"Los empleados con salarios medios (ganan entre 300000 y 100000 pesetas) son: "<<sm<<endl;
    cout <<"Los empleados con salarios bajos (ganan menos de 100000 pesetas) son: "<<sb<<endl;
    cout << endl;
    system("PAUSE");
    return 0;
}   

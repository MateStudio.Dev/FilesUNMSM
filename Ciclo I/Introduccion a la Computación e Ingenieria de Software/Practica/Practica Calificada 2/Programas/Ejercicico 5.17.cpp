#include<iostream>
#include<cstdlib>
using namespace std;
int main()
{
    //declaracion de variables
    int n,i,pr;
    pr=0; //contador de divisores
    cout << "Ejerccio 5.17" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> n;
    cout << endl;
    if (n>1) //condicion del problema
    {
           for(i=1;i<=n;i++) //se evalua la cantidad de divisores que tiene el numero
           {
                        if(n%i==0)
                        {
                          pr+=1;
                        }
           }
           if(pr==2) //si es 2 es primo
           {
             cout << "  * El numero " << n << " es primo" << endl;
           }
           else //de lo contrario es compuesto
           {
             cout << "  * El numero " << n << " es compuesto" << endl;
           }
    }
    else
    {
        cout << "  * Elija numeros mayores a 1" << endl;
    }
    cout << endl;
    system("PAUSE");
    return 0;
}                           

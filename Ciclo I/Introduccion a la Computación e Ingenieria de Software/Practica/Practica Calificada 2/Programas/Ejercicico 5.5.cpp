#include<iostream>
#include<cstdlib>
using namespace std;
int main()
{
    //declaracion de variables
    int i,j,prm;
    prm=0; //contador de divisores
    cout << "Ejerccio 5.5" << endl;
    cout << endl;
    cout << "Los numeros primos comprendidos entre 2 y 1000 son:" << endl;
    cout << endl;
    for(i=2;i<=1000;i++) //numeros comprendidos entre 2 y 1000
    {
        for(j=1;j<=i;j++) //este for evaluara cada valor que tome i con los anteriores
        {
              if(i%j==0) //si son divisores se contaran en la variable prm
              {
                prm += 1; //contador, es lo mismo que prm = prm +1
              }
        }
        if(prm==2) //si solo se cuentan 2 divisores sera primo
        {
          cout << i << endl; //se muestra el numero primo
        }
        prm =0; // se regresa a 0 el contador para ser usado en las siguientes repeticiones
    }
    cout << endl;
    system("PAUSE");
    return 0;
}          

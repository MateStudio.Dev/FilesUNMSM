#include<iostream>
#include<cstdlib>
using namespace std;
int main()
{
    float n,suma;
    cout << "Ejercicio 5.3 - Parte 2" << endl;
    cout << endl;
    suma=1;
    n=1; //inicialización de la variable
    do 
    {
      n++; //variación
      suma=suma+n;
    } 
    while(n<100); //condición
    cout << " * La Suma de Numeros Enteros entre 1 y 100 es: " << suma << endl;
    cout << endl;
    system("PAUSE");
    return 0;
}

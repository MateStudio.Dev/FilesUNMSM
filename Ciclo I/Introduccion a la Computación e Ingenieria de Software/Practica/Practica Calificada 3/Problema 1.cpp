#include<iostream>
#include<cstdlib>
#include<cstring>
using namespace std;
float pro(float,float);
float comp(float x[],int,float);
int main()
{
    int n; 
    float M,suma=0;
    float temp[n];
    cout<<"\nIngrese el numero de total de temperaturas a ingresar: ";
    cin>>n;
    while(cin.fail() || n<=0)
    {
       cin.clear();
       cin.ignore(2000,'\n');
       cout<<"\n � Error: Valor No Aceptable"<<endl;
       cout<<"\nIngrese el numero de total de temperaturas a ingresar: ";
       cin>>n;
    }
    cout<<endl;
    for(int i=0;i<n;i++)
    {
       cout<<"  * Ingrese el valor de la temperatura #"<<i+1<<": ";
       cin>>temp[i]; 
       while(cin.fail())
       {
         cin.clear();
         cin.ignore(2000,'\n');
         cout<<" � Error:Ingrese un Valor Numerico"<<endl;
         cout<<"  * Ingrese el valor de la temperatura #"<<i+1<<": ";
         cin>>temp[i];
       }
       suma=temp[i]+suma;
    }
    M=pro(suma,n);
    cout<<"\nEl Promedio de las Temperaturas es: "<<M<<endl;
    cout<<"\nEl Numero de Temperaturas Mayores o Iguales al Promedio es:  "<<comp(temp,n,M)<<endl;
    cout<<endl;
    system("PAUSE");
    return 0;
}
float pro(float x,float y)
{
    return x/y;
}
float comp(float x[],int num,float pro)
{
    int c=0;
    for (int i=0;i<num;i++)
    {
       if (x[i]>=pro)
       {
          c++;
       }
    }
    return c;   
}

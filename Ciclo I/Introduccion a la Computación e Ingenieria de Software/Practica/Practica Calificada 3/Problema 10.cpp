#include <iostream>
#include <cstdlib>
void leer(float x[],int num);
void ordenar(float x[],int num);
float mediana(float x[],int num);
using namespace std;
int main()
{
    int n;
    float arreglo[100];
    cout<<"\nIngrese el Numero de Datos del arreglo: ";
    cin>>n;
    while(cin.fail() || n<=0)
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<"\n � Error: Valor No Aceptable"<<endl;
      cout<<"\nIngrese el Numero de Datos del arreglo: ";
      cin>>n;
    }
    cout<<endl;
    leer(arreglo,n);
    ordenar(arreglo,n);
    cout<<endl;
    cout<<"\nLa mediana del arreglo es: "<<mediana(arreglo,n)<<endl;
    cout<<endl;
    system("PAUSE");
    return 0;
}
void leer(float x[],int num)
{
     for (int i=0;i<num;i++)
     {
         cout<<"  * Ingrese el dato en la posicion "<<i+1<<" : ";
         cin>>x[i];
         while(cin.fail())
         {
             cin.clear();
             cin.ignore(2000,'\n');
             cout<<"\n � Error: Valor No Aceptable"<<endl;
             cin>>x[i];
         }
     }  
     cout<<endl;  
}
void ordenar(float x[],int num)
{
     float mayor;
     for (int i=0;i<num;i++)
     {
         for(int j=i+1;j<num;j++)
         {
                if (x[i]>x[j])
                {
                              mayor=x[i];
                              x[i]=x[j];
                              x[j]=mayor;
                }
         }
     }
     cout<<endl;
     cout<<"Las notas ordenadas son: ";
     for (int i=0;i<num;i++)
     {
         cout<<x[i]<<" ";
     }
}
float mediana(float x[],int num)
{
     if(num%2==0)
     {
                 return float ((x[(num-2)/2]+x[num/2]))/2;
     }
     if(num%2!=0)
     {
                 return x[(num-1)/2];                 
     }
}

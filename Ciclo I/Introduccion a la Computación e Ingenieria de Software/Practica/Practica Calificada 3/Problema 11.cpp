#include<iostream>
#include<cstdlib>
#include<math.h>
using namespace std;
void coordenadas(float a[],float b[],int);
void convierte_polar(float a[],float b[],int);
int main ()
{    
    int n;
    float Coord_X[n],Coord_Y[n];
    cout<<"\nIngrese la Cantidad de Puntos a registrar: ";
    cin>>n;
    while(cin.fail() || n<=0)
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<"\n � Error: Valor No Aceptable"<<endl;
      cout<<"\nIngrese la Cantidad de Puntos a registrar: ";
      cin>>n;
    }
    cout<<endl;
    coordenadas(Coord_X,Coord_Y,n);
    cout<<"Formulas:";
    cout<<" r = [(x)^2 + (y)^2]^(1/2)"<<endl;
    cout<<"          tetha = tan(y/x)^(-1)"<<endl;
    convierte_polar(Coord_X,Coord_Y,n);
    system("PAUSE");
    return 0;
}
void coordenadas(float a[],float b[],int c)
{
  for(int i=0;i<c;i++)
  {
    cout<<" * Ingrese las coordenadas x , y del punto "<<i+1<<": "<<endl;
    cout<<"   x= ";
    cin>>a[i];
    while(cin.fail() || a[i]==0)
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<" � Error: Valor No Aceptable"<<endl;
      cout<<"   x= ";
      cin>>a[i];
    }
    cout<<"   y= ";
    cin>>b[i];
    while(cin.fail())
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<" � Error: Valor No Aceptable"<<endl;
      cout<<"   y= ";
      cin>>b[i];
    }
  }
  cout<<endl;
}  
void convierte_polar(float a[],float b[],int c)
{
  float r[c];
  float tetha[c];
  for(int i=0;i<c;i++)
  {
     r[i]=sqrt(a[i]*a[i]+b[i]*b[i]);
     tetha[i]=atan(b[i]/a[i]);
  }
  for(int i=0;i<c;i++)
  {
     cout<<"\nLas Coordenadas Polares del Punto #"<<i+1<<" ("<<a[i]<<","<<b[i]<<")"<<":"<<endl;
     cout<<" * r = "<<r[i]<<endl;
     cout<<" * tetha = "<<tetha[i]<<endl;
  }
  cout<<endl;
}  

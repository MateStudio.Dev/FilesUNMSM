#include <iostream>
#include <cstdlib>
void sum(int x[][6],int y[],int z[],int n); /*funcion que halla la suma semestral 
                                              y la suma total de cada vendedor*/
void leer(int x[][6],int n); //lee la matriz donde filas=vendedores y columnas=meses
int mayorm(int y[],int n); //halla el mayor monto total de cada mes
void mayorv(int y[],int n); //halla el mayor y menor monto total de cada vendedor
void promv(int y[],int n); //promedio semestral de cada vendedor
void promm(int y[],int n); //promedio total de cada mes
using namespace std;
int main()
{
    int montos[4][6]; //se crea una matriz que almacenara los montos
    int sumam[6]; //vector que almacena la suma mensual
    int sumav[6]; //vector que almacena la suma semestral de cada vendedor
    leer(montos,4); //se lee el arreglo
    sum(montos,sumam,sumav,4); /*se llama a la funcion que almacenara la suma mensual
                                 y la de cada vendedor en su respectivo array*/
    cout<<endl;
    promm(sumam,6); //promedio mensual
    cout<<endl;
    mayorm(sumam,6); //mayor monto mensual
    cout<<endl;
    promv(sumav,4); //promedio semestral de cada vendedor
    cout<<endl;
    mayorv(sumav,4); //mayor y menor venta resgistrada
    system("PAUSE");
    return 0;
}
void leer(int x[][6],int n) //funcion leer
{
      for (int i=0;i<n;i++)
    {
        cout<<"\nIngrese los montos del vendedor "<<i+1<<endl;
        for(int j=0;j<6;j++)
        {
                cout<<"  * Monto del "<<j+1<<" mes: ";
                cin>>x[i][j];
                while(cin.fail() || x[i][j]==0)
                {
                  cin.clear();
                  cin.ignore(2000,'\n');
                  cout<<" � Error: Valor No Aceptable"<<endl;
                  cout<<"  * Monto del "<<j+1<<" mes: ";
                  cin>>x[i][j];
                }
        }
        cout<<endl;
    }
}  
void sum(int x[][6],int y[],int z[],int n) //funcion suma
{
    
    for (int j=0;j<6;j++) //se halla la suma total mensual
    { 
        y[j]=0;
        for(int i=0;i<n;i++)
        {
                y[j]=y[j]+x[i][j];
        }
    }
    for (int i=0;i<n;i++) //se halla la suma semestral de cada vendedor
    { 
        z[i]=0;
        for(int j=0;j<6;j++)
        {
                z[i]=z[i]+x[i][j];
        }
    }
}    
int mayorm(int y[],int n) //mayor monto mensual
{
    int mes[n]; //array que almacenara la posicion del mes donde se registro la mayor venta
    int mo=0,me=0; //'mo' almacena el mayor monto, 'me' sirve de tama�o para el array mes[n]
    for(int i=0;i<n;i++) // se halla el mayor monto
    {
         if(y[i]>mo)
         {
                   mo=y[i];
         }
    }
    for (int i=0;i<n;i++) //se busca las posiciones del mayor monto
    {
        if(y[i]==mo)
        {
                    mes[me]=i;
                    me++;
        }
    }
    cout<<"El mayor monto se registro en el mes(es) ";
    for (int i=0;i<me;i++) //se imprime las posiciones
    {
        cout<<mes[i]+1<<" ";
    }
    cout<<endl;
    cout<<"con un monto de "<<mo<<endl;
}   
void mayorv(int y[],int n)
{
    int ma,me,l=0,k=0; /*'ma' y 'me' variables que almacenan el mayor y menor monto
                         respectivamente; 'l','k' son tama�os para los arrays pma y pme*/
    int pma[n],pme[n]; //arrays que almacenan la posicion para el mayor y menor monto
    ma=me=y[0];
    for(int i=0;i<n;i++) //se busca el mayor y menor monto
    {
            if(y[i]>ma)
            {
                   ma=y[i];
            }
            if(y[i]<me)
            {
                   me=y[i];
            }
    }
    for (int i=0;i<n;i++) //se busca y almacena las posiciones para el mayor y menor monto
    {
        if(y[i]==ma)
        {
                    pma[l]=i;
                    l++;
        }
        if(y[i]==me)
        {
                    pme[k]=i;
                    k++;
        }
    }
    cout<<"La mayor venta semestral la registro el vendedor(es): ";
    for (int i=0;i<l;i++) //se imprime las posiciones para el mayor monto
    {
        cout<<pma[i]+1<<" ";
    }
    cout<<endl;
    cout<<"con un monto semestral de "<<ma<<endl;
    cout<<endl;
    cout<<"La menor venta semestral la registro el vendedor(es): ";
    for (int i=0;i<k;i++) //se imprime las poscione para el menor monto
    {
        cout<<pme[i]+1<<" ";
    }
    cout<<endl;
    cout<<"con un monto semestral de "<<me<<endl;
    cout<<endl;
}
void promv(int y[],int n) //promedio de cada vendedor 
{
     for(int i=0;i<n;i++)
     {
              cout<<"El promedio semestral del vendedor "<<i+1<<" es: "<<float(y[i])/n<<endl;
     }
}
void promm(int y[],int n) //promedio de cada mes
{
     for(int i=0;i<n;i++)
    {
            cout<<"El promedio de ventas del mes "<<i+1<<" es: "<<float(y[i])/n<<endl;
    }
}

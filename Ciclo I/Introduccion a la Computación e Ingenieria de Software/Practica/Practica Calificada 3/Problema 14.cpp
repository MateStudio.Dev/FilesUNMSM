#include <iostream>
#include <cstdlib>
void leer(int x[][100],int n);
void TriangularInferior(int x[][100],int y[][100],int n);
using namespace std;
int main()
{
    int n;
    cout<<"\nIngrese el Numero de Filas para saber el Tamano de la Matriz Cuadrada: ";
    cin>>n;
    while(cin.fail() || n<1)
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<"\n � Error: Valor No Aceptable"<<endl;
      cout<<"\nIngrese el Numero de Filas para saber el Tamano de la Matriz Cuadrada: ";
      cin>>n;
    }
    int a[n][100],b[n][100];
    cout<<endl;
    leer(a,n);
    TriangularInferior(a,b,n);
    system("PAUSE");
    return 0;
}
void leer(int x[][100],int n)
{
    for (int i=0;i<n;i++)
    {
        cout<<"  * Ingrese los datos de la "<<i+1<<" fila: "<<endl;
        for(int j=0;j<n;j++)
        {
                cin>>x[i][j];
                while(cin.fail())
                {
                  cin.clear();
                  cin.ignore(2000,'\n');
                  cout<<" � Error: Valor No Aceptable"<<endl;
                  cin>>x[i][j];
                }
        }
    cout<<endl;
    }
    cout<<"La Matriz Original es:"<<endl;
    for(int i=0;i<n;i++)
     {
         for(int j=0;j<n;j++)
         {
                cout<<" "<<x[i][j]<<" ";
         }
         cout<<endl;
     }
     cout<<endl;
} 
void TriangularInferior(int x[][100],int y[][100],int n)
{
     if(n!=1)
     {
         for (int i=0;i<n;i++)
         {
             for(int j=0;j<n;j++)
             {
                     y[i][j]=x[i][j];
                     if(i<j)
                     {
                            y[i][j]=0;
                     }
             }
         }
         cout<<"La Matriz Triangular Inferior es:"<<endl;
         for (int i=0;i<n;i++)
         {
             for(int j=0;j<n;j++)
             {
                     cout<<" "<<y[i][j]<<" ";
             }
             cout<<endl;
         }
     }
     else
     {
         cout<<"La matriz 1x1 no tiene Matriz Triangular Inferior"<<endl;
     }
     cout<<endl;   
}

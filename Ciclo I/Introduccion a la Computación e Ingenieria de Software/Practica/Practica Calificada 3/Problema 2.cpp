#include <iostream>
#include <cstdlib>
void leer(float x[],int num);
void ordenar(float x[],int num);
int aprobados(float x[],int num);
using namespace std;
int main()
{
    int n;
    cout<<"\nIngrese cuantas Notas desea ordenar: ";
    cin>>n;
    while(cin.fail() || n<=0)
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<"\n � Error: Valor No Aceptable"<<endl;
      cout<<"\nIngrese cuantas Notas desea ordenar: ";
      cin>>n;
    }
    float notas[100];
    cout<<endl;
    leer(notas,n);
    ordenar(notas,n);
    cout<<"\nLa Cantidad de Aprobados, con Nota Mayor o Igual a 11, son: "<<aprobados(notas,n)<<endl;
    cout<<endl;  
    system("PAUSE");
    return 0;
}
void leer(float x[],int num)
{
     for (int i=0;i<num;i++)
     {
         cout<<"  * Ingrese la nota "<<i+1<<" : ";
         cin>>x[i];
         while(cin.fail())
         {
           cin.clear();
           cin.ignore(2000,'\n');
           cout<<" � Error: Valor No Aceptable"<<endl;
           cout<<"  * Ingrese la nota "<<i+1<<" : ";
           cin>>x[i];
         }
     }
}
void ordenar(float x[],int num)
{
     float mayor;
     for (int i=0;i<num;i++)
     {
         for(int j=i+1;j<num;j++)
         {
                if (x[i]>x[j])
                {
                              mayor=x[i];
                              x[i]=x[j];
                              x[j]=mayor;
                }
         }
     }
     cout<<"\nLas Notas Ordenadas son: ";
     for (int i=0;i<num;i++)
     {
         cout<<x[i]<<" ";
     }
     cout<<endl;
     cout<<"\nLa Menor Nota es: "<<x[0]<<endl;
     cout<<"\nLa Mayor Nota es: "<<x[num-1]<<endl;
}
int aprobados(float x[],int num)
{
    int c=0;
    for (int i=0;i<num;i++)
    {
        if (x[i]>=11)
        {
              c++;
        }
    }
    return c;
}  
     

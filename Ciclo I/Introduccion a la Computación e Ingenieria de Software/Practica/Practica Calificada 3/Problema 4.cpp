#include <iostream>
#include <cstdlib>
int ocurrencias(int v[],int n,int m);
void leer(int v[],int n);
using namespace std;
int main()
{
    int v[100],a,m;
    cout<<"\nIngrese la dimension de arreglo: ";
    cin>>a;
    while(cin.fail() || a<=0)
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<"\n � Error: Valor No Aceptable"<<endl;
      cout<<"\nIngrese la dimension de arreglo: ";
      cin>>a;
    }
    cout<<endl;
    leer(v,a);
    cout<<"\nIngrese el numero que desea encontrar: ";
    cin>>m;
    while(cin.fail())
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<"\n � Error: Valor No Aceptable"<<endl;
      cout<<"\nIngrese el numero que desea encontrar: ";
      cin>>m;
    }    
    cout<<"\n  * El numero a buscar se encuentra "<<ocurrencias(v,a,m)<<" veces"<<endl;
    cout<<endl; 
    system("PAUSE");
    return 0;
}
void leer(int v[],int n)
{
     for (int i=0;i<n;i++)
     {
         cout<<"  * Ingrese el numero de la posicion "<<i+1<<" : ";
         cin>>v[i];
         while(cin.fail())
         {
           cin.clear();
           cin.ignore(2000,'\n');
           cout<<" � Error: Valor No Aceptable"<<endl;
           cout<<"  * Ingrese el numero de la posicion "<<i+1<<" : ";
           cin>>v[i];
         }
     }
}
int ocurrencias(int v[],int n,int m)
{
    int c=0;
    for (int i=0;i<n;i++)
    {
        if(v[i]==m)
        {
                   c++;
        }
    }
    return c;
}
    

#include <iostream>
#include <cstdlib>
void leer(int x[],int num);
void invertir(int x[],int num);
void escribir(int x[],int num);
using namespace std;
int main()
{
    int datos[100],n;
    cout<<"\nIngrese el tamano del arreglo: ";
    cin>>n;
    while(cin.fail() || n<=0)
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<"\n � Error: Valor No Aceptable"<<endl;
      cout<<"\nIngrese el tamano del arreglo: ";
      cin>>n;
    }
    cout<<endl;
    leer(datos,n);
    escribir(datos,n);
    cout<<endl;
    invertir(datos,n);
    cout<<endl;
    system("PAUSE");
    return 0;
}
void leer(int x[],int num)
{
     for (int i=0;i<num;i++)
     {
         cout<<"  * Ingrese el dato en la posicion "<<i+1<<" : ";
         cin>>x[i];
         while(cin.fail())
         {
           cin.clear();
           cin.ignore(2000,'\n');
           cout<<" � Error: Valor No Aceptable"<<endl;
           cout<<"  * Ingrese el dato en la posicion "<<i+1<<" : ";
           cin>>x[i];
         }
     }
     cout<<endl;
}
void escribir(int x[],int num)
{
     cout<<"El arreglo original es el siguiente: "<<endl;
     for (int i=0;i<num;i++)
     {
     
         cout<<x[i]<<" ";
     }
     cout<<endl;
}
void invertir(int x[],int num)
{
     cout<<"El arreglo invertido es el siguiente: "<<endl;
     for (int i=num-1;i>=0;i--)
     {
     
         cout<<x[i]<<" ";
     }
     cout<<endl;
}

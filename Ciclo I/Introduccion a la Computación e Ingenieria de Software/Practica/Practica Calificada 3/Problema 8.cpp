#include <iostream>
#include <cstdlib>
bool verificaArrays(int x[],int y[],int num);
void leer(int x[],int num);
using namespace std;
int main()
{
    int a[100],b[100],n;
    cout<<"\nIngrese el tamano de los arreglos: ";
    cin>>n;
    while(cin.fail() || n<=0)
    {
      cin.clear();
      cin.ignore(2000,'\n');
      cout<<"\n � Error: Valor No Aceptable"<<endl;
      cout<<"\nIngrese el tamano de los arreglos: ";
      cin>>n;
    }
    cout<<endl;
    cout<<"Ingrese los datos del primer arreglo: "<<endl;
    leer(a,n);
    cout<<endl;
    cout<<"Ingrese los datos del segundo arreglo: "<<endl;
    leer(b,n);
    cout<<endl;
    if(verificaArrays(a,b,n))
    {
        cout<<"Los elementos del segundo arreglo se encuentran en orden inverso"<<endl;
    }
    else
    {
        cout<<"Los elementos del segundo arreglo no se encuentran en orden inverso"<<endl;
    }
    cout<<endl;
    system("PAUSE");
    return 0;
}
void leer(int x[],int num)
{
    for (int i=0;i<num;i++)
    {
        cout<<"  * Ingrese dato en la posicion "<<i+1<<" : ";
        cin>>x[i];
        while(cin.fail())
        {
            cin.clear();
            cin.ignore(2000,'\n');
            cout<<" � Error: Valor No Aceptable"<<endl;
            cout<<"  * Ingrese dato en la posicion "<<i+1<<" : ";
            cin>>x[i];
        }
    }
}
bool verificaArrays(int x[],int y[],int num)
{
    bool a;
    for(int i=0;i<num;i++)
    {
       a=false;
       if(x[i]==y[num-1-i])
       {
          a= true;
       }
       else
       {
          break;
       }             
    }
    return a;
}

@echo off
for /f "tokens=1,2,3,4* delims=/, " %%a in ('date /t') do set fecha=%%d%%c%%b
title "Carpetas para Release"
echo Generando Carpetas
mkdir "MD_%fecha%"
CD "MD_%fecha%"
mkdir Ejecutables
mkdir Scripts
mkdir Documentos
mkdir Rollback

echo Generando archivo excel_ms.xls
PAUSE

echo 	" "							>>	excel_ms.xls
echo 		FORMATO DE PASE A PRODUCION 			>>	excel_ms.xls
echo 	" "							>>	excel_ms.xls
echo 	Sistema	Nombre del Sistema			Fecha	%fecha%	>> 	excel_ms.xls
echo	" "							>>	excel_ms.xls
echo 	Modulo	Nombre del Modulo			 	>> 	excel_ms.xls
echo	" "							>>	excel_ms.xls
echo 	Reponsable	Nombre del Responsable			Firma	>> 	excel_ms.xls
echo	" "							>>	excel_ms.xls
echo 	Jefe Inmediato	Nombre del Jefe 			Firma	>> 	excel_ms.xls
echo	" "							>>	excel_ms.xls
echo	" "							>>	excel_ms.xls
echo 	Ejecutables:						>>	excel_ms.xls
echo 	Ejecutable	Tipo	Ubicacion			>>	excel_ms.xls
echo 	""	""	""					>>	excel_ms.xls
echo 	""	""	""					>>	excel_ms.xls
echo 	""	""	""					>>	excel_ms.xls
echo 	""	""	""					>>	excel_ms.xls
echo 	Scripts:						>>	excel_ms.xls
echo 	""	""	""					>>	excel_ms.xls
echo 	""	""	""					>>	excel_ms.xls
echo 	""	""	""					>>	excel_ms.xls
echo 	""	""	""					>>	excel_ms.xls
PAUSE
exit
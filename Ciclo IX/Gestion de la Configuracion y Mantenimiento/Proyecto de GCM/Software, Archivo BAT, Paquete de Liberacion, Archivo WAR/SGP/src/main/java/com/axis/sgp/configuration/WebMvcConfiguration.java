package com.axis.sgp.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.axis.sgp.component.RequestTimeInterceptor;
import com.axis.sgp.service.form.JobPositionFormatter;
import com.axis.sgp.service.form.ScheduleFormatter;

@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {

	@Autowired
	@Qualifier("requestTimeInterceptor")
	private RequestTimeInterceptor requestTimeInterceptor;

	@Autowired
	@Qualifier("scheduleFormatter")
	private ScheduleFormatter scheduleFormatter;
	
	@Autowired
	@Qualifier("jobpositionFormatter")
	private JobPositionFormatter jobpositionFormatter;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(requestTimeInterceptor);
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(scheduleFormatter);
		registry.addFormatter(jobpositionFormatter);
	}

}
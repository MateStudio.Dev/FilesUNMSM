package com.axis.sgp.constant;

public class ViewConstant {
	
	public static final String LOGIN_VIEW = "login";
	public static final String LIST_VIEW = "list";
	public static final String REGISTER_VIEW = "register";
	public static final String FIND_VIEW = "find";
	public static final String UPDATE_VIEW = "update";
	
}

package com.axis.sgp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.axis.sgp.constant.ViewConstant;
import com.axis.sgp.model.UserCredential;
import com.axis.sgp.model.UserModel;
import com.axis.sgp.service.UserService;

@Controller
public class LoginController {

	public static final Log LOG = LogFactory.getLog(LoginController.class);

	@Autowired
	@Qualifier("userServiceImpl")
	private UserService userService;

	private UserModel userTemp;

	@GetMapping("/")
	public String redirectToLogin() {
		LOG.info("METHOD: redirectToLogin()");
		LOG.info("Redirec to LOGIN_VIEW (login)");
		return "redirect:/login";
	}

	@GetMapping("/login")
	public String showLoginForm(Model model, @RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {
		LOG.info("METHOD: showLoginForm() -- PARAMS: error: " + error + ", logout: " + logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
		model.addAttribute("userCredentials", new UserCredential());
		LOG.info("Returning to LOGIN_VIEW (login)");
		return ViewConstant.LOGIN_VIEW;
	}

	@PostMapping("/logincheck")
	public String loginCheck(@ModelAttribute(name = "userCredentials") UserCredential userCredential) {
		LOG.info("METHOD: showLoginForm() -- PARAMS: " + userCredential.toString());
		userTemp = userService.findUserByEmail(userCredential.getUsername());
		if (userTemp != null && userCredential.getPassword().equals(userTemp.getDni()) && userTemp.isIsadmin() == true
				&& userTemp.isIsactive() == true) {
			LOG.info("Returning to LIST_VIEW (listusers)");
			return "redirect:/users/listusers";
		}
		LOG.info("Redirec to LOGIN_VIEW (login?error)");
		return "redirect:/login?error";
	}

}

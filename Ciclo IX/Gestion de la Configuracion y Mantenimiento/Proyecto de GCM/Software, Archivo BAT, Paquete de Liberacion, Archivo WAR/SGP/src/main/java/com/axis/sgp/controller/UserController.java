package com.axis.sgp.controller;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.axis.sgp.constant.ViewConstant;
import com.axis.sgp.model.UserModel;
import com.axis.sgp.service.JobPositionService;
import com.axis.sgp.service.ScheduleService;
import com.axis.sgp.service.UserService;

@Controller
@RequestMapping("/users")
public class UserController {

	public static final Log LOG = LogFactory.getLog(UserController.class);
	public Calendar fecha = new GregorianCalendar();
	public String dia, mes;

	@Autowired
	@Qualifier("userServiceImpl")
	private UserService userService;

	@Autowired
	@Qualifier("jobpositionServiceImpl")
	private JobPositionService jobpositionService;

	@Autowired
	@Qualifier("scheduleServiceImpl")
	private ScheduleService scheduleService;

	@GetMapping("/userform")
	public String redirectUserForm(Model model) {
		LOG.info("METHOD: redirectUserForm()");
		model.addAttribute("userModel", new UserModel());
		model.addAttribute("jobpositions", jobpositionService.listAllJobPositions());
		model.addAttribute("schedules", scheduleService.listAllSchedules());
		LOG.info("Returning to REGISTER_VIEW (adduser)");
		return ViewConstant.REGISTER_VIEW;
	}
	
	@GetMapping("/updateform/{id}")
	public String redirectUpdateForm(Model model, @PathVariable("id") Integer id) {
		LOG.info("METHOD: redirectUpdateForm()");
		model.addAttribute("userModel", userService.findUserByIduser(id));
		model.addAttribute("jobpositions", jobpositionService.listAllJobPositions());
		model.addAttribute("schedules", scheduleService.listAllSchedules());
		LOG.info("Returning to UPDATE_VIEW (updateuser)");
		return ViewConstant.UPDATE_VIEW;
	}
	
	@GetMapping("/finduser")
	public ModelAndView findUsers() {
		LOG.info("METHOD: findUsers()");
		ModelAndView mav = new ModelAndView(ViewConstant.FIND_VIEW);
		mav.addObject("users", userService.listAllUsers());
		LOG.info("Returning to FIND_VIEW (finduser)");
		return mav;
	}
	
	@PostMapping("/updateuser/{id}")
	public String updateUser(Model model, @ModelAttribute(name = "userModel") UserModel userModel, @PathVariable("id") Integer id) {
		LOG.info("METHOD: updateUSer() -- PARAMS:" + userModel.toString());
		userModel.setIduser(id);
		if (userService.updateUser(userModel) != null) {
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}
		LOG.info("Returning to LIST_VIEW (listusers)");
		return "redirect:/users/listusers";
	}

	@PostMapping("/adduser")
	public String addUser(Model model, @ModelAttribute(name = "userModel") UserModel userModel) {
		LOG.info("METHOD: addUSer() -- PARAMS:" + userModel.toString());
		userModel.setHiredate(getCurrentDate());
		userModel.setIsactive(true);
		if (userService.addUser(userModel) != null) {
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}
		LOG.info("Returning to LIST_VIEW (listusers)");
		return "redirect:/users/listusers";
	}

	@GetMapping("/listusers")
	public ModelAndView listarUsers() {
		LOG.info("METHOD: listarUsers()");
		ModelAndView mav = new ModelAndView(ViewConstant.LIST_VIEW);
		mav.addObject("users", userService.listAllUsers());
		LOG.info("Returning to LIST_VIEW (listusers)");
		return mav;
	}

	public String getCurrentDate() {
		dia = (fecha.get(Calendar.DAY_OF_MONTH) > 9) ? String.valueOf(fecha.get(Calendar.DAY_OF_MONTH))
				: "0" + String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
		mes = (fecha.get(Calendar.MONTH) + 1 > 9) ? String.valueOf(fecha.get(Calendar.MONTH) + 1)
				: "0" + String.valueOf(fecha.get(Calendar.MONTH) + 1);
		return dia + "/" + mes + "/" + String.valueOf(fecha.get(Calendar.YEAR));
	}

}
package com.axis.sgp.converter;

import org.springframework.stereotype.Component;

import com.axis.sgp.entity.Area;
import com.axis.sgp.model.AreaModel;

@Component("areaConverter")
public class AreaConverter {

	public Area convertAreaModel2Area(AreaModel areaModel) {
		if (areaModel != null) {
			Area area = new Area();
			area.setIdarea(areaModel.getIdarea());
			area.setName(areaModel.getName());
			area.setDescription(areaModel.getDescription());
			area.setIddepartment(areaModel.getIddepartment());
			area.setListjobpositions(areaModel.getListjobpositions());
			return area;
		}
		return null;
	}

	public AreaModel convertArea2AreaModel(Area area) {
		if (area != null) {
			AreaModel areaModel = new AreaModel();
			areaModel.setIdarea(area.getIdarea());
			areaModel.setName(area.getName());
			areaModel.setDescription(area.getDescription());
			areaModel.setIddepartment(area.getIddepartment());
			areaModel.setListjobpositions(area.getListjobpositions());
			return areaModel;
		}
		return null;
	}

}

package com.axis.sgp.converter;

import org.springframework.stereotype.Component;

import com.axis.sgp.entity.Department;
import com.axis.sgp.model.DepartmentModel;

@Component("departmentConverter")
public class DepartmentConverter {

	public Department convertDepartmentModel2Department(DepartmentModel departmentModel) {
		if (departmentModel != null) {
			Department department = new Department();
			department.setIddepartment(departmentModel.getIddepartment());
			department.setName(departmentModel.getName());
			department.setDescription(departmentModel.getDescription());
			department.setListareas(departmentModel.getListareas());
			return department;
		}
		return null;
	}

	public DepartmentModel convertDepartment2DepartmentModel(Department department) {
		if (department != null) {
			DepartmentModel departmentModel = new DepartmentModel();
			departmentModel.setIddepartment(department.getIddepartment());
			departmentModel.setName(department.getName());
			departmentModel.setDescription(department.getDescription());
			departmentModel.setListareas(department.getListareas());
			return departmentModel;
		}
		return null;
	}

}

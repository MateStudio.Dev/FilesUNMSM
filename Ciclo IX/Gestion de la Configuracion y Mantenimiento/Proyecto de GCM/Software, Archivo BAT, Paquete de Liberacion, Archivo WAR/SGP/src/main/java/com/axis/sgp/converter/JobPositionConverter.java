package com.axis.sgp.converter;

import org.springframework.stereotype.Component;

import com.axis.sgp.entity.JobPosition;
import com.axis.sgp.model.JobPositionModel;

@Component("jobpositionConverter")
public class JobPositionConverter {

	public JobPosition convertJobPositionModel2JobPosition(JobPositionModel jobpositionModel) {
		if (jobpositionModel != null) {
			JobPosition jobposition = new JobPosition();
			jobposition.setIdjobposition(jobpositionModel.getIdjobposition());
			jobposition.setName(jobpositionModel.getName());
			jobposition.setDescription(jobpositionModel.getDescription());
			jobposition.setIdarea(jobpositionModel.getIdarea());
			jobposition.setListusers(jobpositionModel.getListusers());
			return jobposition;
		}
		return null;
	}

	public JobPositionModel convertJobPosition2JobPositionModel(JobPosition jobposition) {
		if (jobposition != null) {
			JobPositionModel jobpositionModel = new JobPositionModel();
			jobpositionModel.setIdjobposition(jobposition.getIdjobposition());
			jobpositionModel.setName(jobposition.getName());
			jobpositionModel.setDescription(jobposition.getDescription());
			jobpositionModel.setIdarea(jobposition.getIdarea());
			jobpositionModel.setListusers(jobposition.getListusers());
			return jobpositionModel;
		}
		return null;
	}

}

package com.axis.sgp.converter;

import org.springframework.stereotype.Component;

import com.axis.sgp.entity.Schedule;
import com.axis.sgp.model.ScheduleModel;

@Component("scheduleConverter")
public class ScheduleConverter {

	public Schedule convertScheduleModel2Schedule(ScheduleModel scheduleModel) {
		if (scheduleModel != null) {
			Schedule schedule = new Schedule();
			schedule.setIdschedule(scheduleModel.getIdschedule());
			schedule.setStarttime(scheduleModel.getStarttime());
			schedule.setFinaltime(scheduleModel.getFinaltime());
			schedule.setTurn(scheduleModel.getTurn());
			schedule.setListusers(scheduleModel.getListusers());
			return schedule;
		}
		return null;
	}

	public ScheduleModel convertSchedule2ScheduleModel(Schedule schedule) {
		if (schedule != null) {
			ScheduleModel scheduleModel = new ScheduleModel();
			scheduleModel.setIdschedule(schedule.getIdschedule());
			scheduleModel.setStarttime(schedule.getStarttime());
			scheduleModel.setFinaltime(schedule.getFinaltime());
			scheduleModel.setTurn(schedule.getTurn());
			scheduleModel.setListusers(schedule.getListusers());
			return scheduleModel;
		}
		return null;
	}

}

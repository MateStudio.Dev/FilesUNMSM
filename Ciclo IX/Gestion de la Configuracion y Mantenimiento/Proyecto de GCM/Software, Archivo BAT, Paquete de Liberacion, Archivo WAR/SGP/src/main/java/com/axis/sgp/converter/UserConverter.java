package com.axis.sgp.converter;

import org.springframework.stereotype.Component;

import com.axis.sgp.entity.User;
import com.axis.sgp.model.UserModel;

@Component("userConverter")
public class UserConverter {

	public User convertUserModel2User(UserModel userModel) {
		if (userModel != null) {
			User user = new User();
			user.setIduser(userModel.getIduser());
			user.setDni(userModel.getDni());
			user.setFirstname(userModel.getFirstname());
			user.setLastname(userModel.getLastname());
			user.setAddress(userModel.getAddress());
			user.setBirthdate(userModel.getBirthdate());
			user.setGender(userModel.getGender());
			user.setEmail(userModel.getEmail());
			user.setPhonenumber(userModel.getPhonenumber());
			user.setHiredate(userModel.getHiredate());
			user.setIsactive(userModel.isIsactive());
			user.setIsadmin(userModel.isIsadmin());
			user.setIdschedule(userModel.getIdschedule());
			user.setIdjobposition(userModel.getIdjobposition());
			return user;
		}
		return null;
	}

	public UserModel convertUser2UserModel(User user) {
		if (user != null) {
			UserModel userModel = new UserModel();
			userModel.setIduser(user.getIduser());
			userModel.setDni(user.getDni());
			userModel.setFirstname(user.getFirstname());
			userModel.setLastname(user.getLastname());
			userModel.setAddress(user.getAddress());
			userModel.setBirthdate(user.getBirthdate());
			userModel.setGender(user.getGender());
			userModel.setEmail(user.getEmail());
			userModel.setPhonenumber(user.getPhonenumber());
			userModel.setHiredate(user.getHiredate());
			userModel.setIsactive(user.isIsactive());
			userModel.setIsadmin(user.isIsadmin());
			userModel.setIdschedule(user.getIdschedule());
			userModel.setIdjobposition(user.getIdjobposition());
			return userModel;
		}
		return null;
	}

}

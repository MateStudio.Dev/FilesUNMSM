package com.axis.sgp.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "area")
public class Area implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "idarea", length = 4)
	private Integer idarea;

	@NotNull
	@Column(name = "name", length = 50)
	private String name;

	@Column(name = "description", length = 100)
	private String description;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "iddepartment")
	private Department iddepartment;

	@OneToMany(mappedBy = "idarea", cascade = CascadeType.ALL)
	private List<JobPosition> listjobpositions;

	public Area() {
	}

	public Area(Integer idarea, String name, String description, Department iddepartment) {
		this.idarea = idarea;
		this.name = name;
		this.description = description;
		this.iddepartment = iddepartment;
	}

	public Integer getIdarea() {
		return idarea;
	}

	public void setIdarea(Integer idarea) {
		this.idarea = idarea;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Department getIddepartment() {
		return iddepartment;
	}

	public void setIddepartment(Department iddepartment) {
		this.iddepartment = iddepartment;
	}

	public List<JobPosition> getListjobpositions() {
		return listjobpositions;
	}

	public void setListjobpositions(List<JobPosition> listjobpositions) {
		this.listjobpositions = listjobpositions;
	}

	@Override
	public String toString() {
		return "Area [idarea=" + idarea + ", name=" + name + ", description=" + description + ", iddepartment="
				+ iddepartment + "]";
	}

}

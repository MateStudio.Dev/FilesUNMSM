package com.axis.sgp.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "department")
public class Department implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "iddepartment", length = 4)
	private Integer iddepartment;

	@NotNull
	@Column(name = "name", length = 50)
	private String name;

	@Column(name = "description", length = 100)
	private String description;

	@OneToMany(mappedBy = "iddepartment", cascade = CascadeType.ALL)
	private List<Area> listareas;

	public Department() {
	}

	public Department(Integer iddepartment, String name, String description) {
		this.iddepartment = iddepartment;
		this.name = name;
		this.description = description;
	}

	public Integer getIddepartment() {
		return iddepartment;
	}

	public void setIddepartment(Integer iddepartment) {
		this.iddepartment = iddepartment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Area> getListareas() {
		return listareas;
	}

	public void setListareas(List<Area> listareas) {
		this.listareas = listareas;
	}

	@Override
	public String toString() {
		return "Department [iddepartment=" + iddepartment + ", name=" + name + ", description=" + description + "]";
	}

}

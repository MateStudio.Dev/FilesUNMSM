package com.axis.sgp.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "jobposition")
public class JobPosition implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "idjobposition", length = 4)
	private Integer idjobposition;

	@NotNull
	@Column(name = "name", length = 50)
	private String name;

	@NotNull
	@Column(name = "description", length = 100)
	private String description;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idarea")
	private Area idarea;

	@OneToMany(mappedBy = "idjobposition", cascade = CascadeType.ALL)
	private List<User> listusers;

	public JobPosition() {
	}

	public JobPosition(Integer idjobposition, String name, String description, Area idarea) {
		this.idjobposition = idjobposition;
		this.name = name;
		this.description = description;
		this.idarea = idarea;
	}

	public Integer getIdjobposition() {
		return idjobposition;
	}

	public void setIdjobposition(Integer idjobposition) {
		this.idjobposition = idjobposition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Area getIdarea() {
		return idarea;
	}

	public void setIdarea(Area idarea) {
		this.idarea = idarea;
	}

	public List<User> getListusers() {
		return listusers;
	}

	public void setListusers(List<User> listusers) {
		this.listusers = listusers;
	}

	@Override
	public String toString() {
		return "JobPosition [idjobposition=" + idjobposition + ", name=" + name + ", description=" + description
				+ ", idarea=" + idarea + "]";
	}

}

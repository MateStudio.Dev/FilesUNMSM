package com.axis.sgp.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "schedule")
public class Schedule implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "idschedule", length = 4)
	private Integer idschedule;

	@NotNull
	@Column(name = "starttime", length = 5)
	private String starttime;

	@NotNull
	@Column(name = "finaltime", length = 5)
	private String finaltime;

	@NotNull
	@Column(name = "turn", length = 10)
	private String turn;

	@OneToMany(mappedBy = "idschedule", cascade = CascadeType.ALL)
	private List<User> listusers;

	public Schedule() {
	}

	public Schedule(Integer idschedule, String starttime, String finaltime, String turn) {
		this.idschedule = idschedule;
		this.starttime = starttime;
		this.finaltime = finaltime;
		this.turn = turn;
	}

	public Integer getIdschedule() {
		return idschedule;
	}

	public void setIdschedule(Integer idschedule) {
		this.idschedule = idschedule;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getFinaltime() {
		return finaltime;
	}

	public void setFinaltime(String finaltime) {
		this.finaltime = finaltime;
	}

	public String getTurn() {
		return turn;
	}

	public void setTurn(String turn) {
		this.turn = turn;
	}

	public List<User> getListusers() {
		return listusers;
	}

	public void setListusers(List<User> listusers) {
		this.listusers = listusers;
	}

	@Override
	public String toString() {
		return "Schedule [idschedule=" + idschedule + ", starttime=" + starttime + ", finaltime=" + finaltime
				+ ", turn=" + turn + "]";
	}

}

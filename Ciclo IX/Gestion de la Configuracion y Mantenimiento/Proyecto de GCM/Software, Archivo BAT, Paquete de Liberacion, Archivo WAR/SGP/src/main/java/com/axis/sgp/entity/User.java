package com.axis.sgp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "iduser", length = 4)
	private Integer iduser;

	@Column(name = "dni", length = 8)
	private String dni;

	@Column(name = "firstname", length = 50)
	private String firstname;

	@Column(name = "lastname", length = 50)
	private String lastname;

	@Column(name = "address", length = 100)
	private String address;

	@Column(name = "birthdate", length = 50)
	private String birthdate;

	@Column(name = "gender", length = 1)
	private String gender;

	@Column(name = "email", length = 50)
	private String email;

	@Column(name = "phone_number", length = 50)
	private String phonenumber;

	@Column(name = "hire_date", length = 50)
	private String hiredate;

	@Column(name = "is_active", length = 1)
	private boolean isactive;

	@Column(name = "is_admin", length = 1)
	private boolean isadmin;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idschedule")
	private Schedule idschedule;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idjobposition")
	private JobPosition idjobposition;

	public User() {
	}

	public User(Integer iduser, String dni, String firstname, String lastname, String address, String birthdate,
			String gender, String email, String phonenumber, String hiredate, boolean isactive, boolean isadmin,
			Schedule idschedule, JobPosition idjobposition) {
		this.iduser = iduser;
		this.dni = dni;
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.birthdate = birthdate;
		this.gender = gender;
		this.email = email;
		this.phonenumber = phonenumber;
		this.hiredate = hiredate;
		this.isactive = isactive;
		this.isadmin = isadmin;
		this.idschedule = idschedule;
		this.idjobposition = idjobposition;
	}

	public Integer getIduser() {
		return iduser;
	}

	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getHiredate() {
		return hiredate;
	}

	public void setHiredate(String hiredate) {
		this.hiredate = hiredate;
	}

	public boolean isIsactive() {
		return isactive;
	}

	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}

	public boolean isIsadmin() {
		return isadmin;
	}

	public void setIsadmin(boolean isadmin) {
		this.isadmin = isadmin;
	}

	public Schedule getIdschedule() {
		return idschedule;
	}

	public void setIdschedule(Schedule idschedule) {
		this.idschedule = idschedule;
	}

	public JobPosition getIdjobposition() {
		return idjobposition;
	}

	public void setIdjobposition(JobPosition idjobposition) {
		this.idjobposition = idjobposition;
	}

	@Override
	public String toString() {
		return "User [iduser=" + iduser + ", dni=" + dni + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", address=" + address + ", birthdate=" + birthdate + ", gender=" + gender + ", email=" + email
				+ ", phonenumber=" + phonenumber + ", hiredate=" + hiredate + ", isactive=" + isactive + ", isadmin="
				+ isadmin + ", idschedule=" + idschedule + ", idjobposition=" + idjobposition + "]";
	}

}

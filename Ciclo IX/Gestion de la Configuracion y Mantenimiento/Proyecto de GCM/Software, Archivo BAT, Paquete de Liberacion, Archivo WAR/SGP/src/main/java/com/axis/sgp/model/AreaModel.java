package com.axis.sgp.model;

import java.util.List;

import com.axis.sgp.entity.Department;
import com.axis.sgp.entity.JobPosition;

public class AreaModel {

	private Integer idarea;
	private String name;
	private String description;
	private Department iddepartment;
	private List<JobPosition> listjobpositions;

	public AreaModel() {
	}

	public AreaModel(Integer idarea, String name, String description, Department iddepartment) {
		this.idarea = idarea;
		this.name = name;
		this.description = description;
		this.iddepartment = iddepartment;
	}

	public Integer getIdarea() {
		return idarea;
	}

	public void setIdarea(Integer idarea) {
		this.idarea = idarea;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Department getIddepartment() {
		return iddepartment;
	}

	public void setIddepartment(Department iddepartment) {
		this.iddepartment = iddepartment;
	}

	public List<JobPosition> getListjobpositions() {
		return listjobpositions;
	}

	public void setListjobpositions(List<JobPosition> listjobpositions) {
		this.listjobpositions = listjobpositions;
	}

	@Override
	public String toString() {
		return "AreaModel [idarea=" + idarea + ", name=" + name + ", description=" + description + ", iddepartment="
				+ iddepartment + "]";
	}

}

package com.axis.sgp.model;

import java.util.List;

import com.axis.sgp.entity.Area;

public class DepartmentModel {

	private Integer iddepartment;
	private String name;
	private String description;
	private List<Area> listareas;

	public DepartmentModel() {
	}

	public DepartmentModel(Integer iddepartment, String name, String description) {
		this.iddepartment = iddepartment;
		this.name = name;
		this.description = description;
	}

	public Integer getIddepartment() {
		return iddepartment;
	}

	public void setIddepartment(Integer iddepartment) {
		this.iddepartment = iddepartment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Area> getListareas() {
		return listareas;
	}

	public void setListareas(List<Area> listareas) {
		this.listareas = listareas;
	}

	@Override
	public String toString() {
		return "DepartmentModel [iddepartment=" + iddepartment + ", name=" + name + ", description=" + description
				+ "]";
	}

}

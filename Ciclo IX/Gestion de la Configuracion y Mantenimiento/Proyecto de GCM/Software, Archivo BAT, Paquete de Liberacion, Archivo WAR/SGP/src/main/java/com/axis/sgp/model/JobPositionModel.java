package com.axis.sgp.model;

import java.util.List;

import com.axis.sgp.entity.Area;
import com.axis.sgp.entity.User;

public class JobPositionModel {

	private Integer idjobposition;
	private String name;
	private String description;
	private Area idarea;
	private List<User> listusers;

	public JobPositionModel() {
	}

	public JobPositionModel(Integer idjobposition, String name, String description, Area idarea) {
		this.idjobposition = idjobposition;
		this.name = name;
		this.description = description;
		this.idarea = idarea;
	}

	public Integer getIdjobposition() {
		return idjobposition;
	}

	public void setIdjobposition(Integer idjobposition) {
		this.idjobposition = idjobposition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Area getIdarea() {
		return idarea;
	}

	public void setIdarea(Area idarea) {
		this.idarea = idarea;
	}

	public List<User> getListusers() {
		return listusers;
	}

	public void setListusers(List<User> listusers) {
		this.listusers = listusers;
	}

	@Override
	public String toString() {
		return "JobPositionModel [idjobposition=" + idjobposition + ", name=" + name + ", description=" + description
				+ ", idarea=" + idarea + "]";
	}

}

package com.axis.sgp.model;

import java.util.List;

import com.axis.sgp.entity.User;

public class ScheduleModel {

	private Integer idschedule;
	private String starttime;
	private String finaltime;
	private String turn;
	private List<User> listusers;

	public ScheduleModel() {
	}

	public ScheduleModel(Integer idschedule, String starttime, String finaltime, String turn) {
		this.idschedule = idschedule;
		this.starttime = starttime;
		this.finaltime = finaltime;
		this.turn = turn;
	}

	public Integer getIdschedule() {
		return idschedule;
	}

	public void setIdschedule(Integer idschedule) {
		this.idschedule = idschedule;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getFinaltime() {
		return finaltime;
	}

	public void setFinaltime(String finaltime) {
		this.finaltime = finaltime;
	}

	public String getTurn() {
		return turn;
	}

	public void setTurn(String turn) {
		this.turn = turn;
	}

	public List<User> getListusers() {
		return listusers;
	}

	public void setListusers(List<User> listusers) {
		this.listusers = listusers;
	}

	@Override
	public String toString() {
		return "ScheduleModel [idschedule=" + idschedule + ", starttime=" + starttime + ", finaltime=" + finaltime
				+ ", turn=" + turn + "]";
	}

}

package com.axis.sgp.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.axis.sgp.entity.Area;

@Repository("areaRepository")
public interface AreaRepository extends JpaRepository<Area, Serializable> {

	public abstract Area findByIdarea(Integer idarea);

}

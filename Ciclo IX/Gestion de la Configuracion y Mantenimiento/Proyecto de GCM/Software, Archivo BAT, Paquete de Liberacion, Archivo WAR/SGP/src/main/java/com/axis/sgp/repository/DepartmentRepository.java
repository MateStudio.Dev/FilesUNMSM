package com.axis.sgp.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.axis.sgp.entity.Department;

@Repository("departmentRepository")
public interface DepartmentRepository extends JpaRepository<Department, Serializable> {

	public abstract Department findByIddepartment(Integer iddepartment);

}

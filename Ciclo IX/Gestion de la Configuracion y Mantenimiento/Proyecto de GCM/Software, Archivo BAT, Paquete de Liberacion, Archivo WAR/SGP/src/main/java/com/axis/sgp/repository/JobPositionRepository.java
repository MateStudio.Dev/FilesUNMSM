package com.axis.sgp.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.axis.sgp.entity.JobPosition;

@Repository("jobpositionRepository")
public interface JobPositionRepository extends JpaRepository<JobPosition, Serializable> {

	public abstract JobPosition findByIdjobposition(Integer idjobposition);

}

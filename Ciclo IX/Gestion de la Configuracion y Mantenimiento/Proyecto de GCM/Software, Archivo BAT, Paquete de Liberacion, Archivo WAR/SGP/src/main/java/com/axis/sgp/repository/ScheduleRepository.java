package com.axis.sgp.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.axis.sgp.entity.Schedule;

@Repository("scheduleRepository")
public interface ScheduleRepository extends JpaRepository<Schedule, Serializable> {

	public abstract Schedule findByIdschedule(Integer idschedule);

}

package com.axis.sgp.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.axis.sgp.entity.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Serializable> {

	public abstract User findByDni(String dni);

	public abstract User findByEmail(String email);
	
	public abstract User findByIduser(Integer iduser);

}

package com.axis.sgp.service;

import com.axis.sgp.entity.Department;

public interface DepartmentService {

	public abstract Department findDepartmentByIddepartment(Integer iddepartment);

}

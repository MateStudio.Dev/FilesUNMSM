package com.axis.sgp.service;

import java.util.List;

import com.axis.sgp.entity.JobPosition;

public interface JobPositionService {

	public abstract JobPosition findJobPositionByIdjobposition(Integer idjobposition);

	public abstract List<JobPosition> listAllJobPositions();

}

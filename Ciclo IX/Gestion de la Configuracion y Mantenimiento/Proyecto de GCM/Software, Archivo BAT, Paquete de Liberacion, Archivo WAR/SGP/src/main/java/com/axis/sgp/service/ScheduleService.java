package com.axis.sgp.service;

import java.util.List;

import com.axis.sgp.entity.Schedule;

public interface ScheduleService {

	public abstract List<Schedule> listAllSchedules();

	public abstract Schedule findScheduleByIdschedule(Integer idschedule);

}

package com.axis.sgp.service;

import java.util.List;

import com.axis.sgp.model.UserModel;

public interface UserService {

	public abstract UserModel addUser(UserModel userModel);
	
	public abstract UserModel updateUser(UserModel userModel);

	public abstract List<UserModel> listAllUsers();

	public abstract UserModel findUserByDni(String dni);

	public abstract void removeUser(String dni);

	public abstract UserModel findUserByEmail(String email);
	
	public abstract UserModel findUserByIduser(Integer iduser);

}

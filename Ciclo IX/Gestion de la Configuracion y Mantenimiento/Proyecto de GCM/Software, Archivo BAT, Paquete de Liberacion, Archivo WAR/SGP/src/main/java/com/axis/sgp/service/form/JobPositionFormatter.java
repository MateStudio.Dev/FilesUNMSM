package com.axis.sgp.service.form;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Service;

import com.axis.sgp.entity.JobPosition;
import com.axis.sgp.service.JobPositionService;

@Service("jobpositionFormatter")
public class JobPositionFormatter implements Formatter<JobPosition> {
	
	@Autowired
	@Qualifier("jobpositionServiceImpl")
	private JobPositionService jobpositionService;

	@Override
	public String print(final JobPosition object, final Locale locate) {
		return (object != null ? object.getIdjobposition().toString() : "");
	}

	@Override
	public JobPosition parse(final String text, final Locale locate) throws ParseException {
		final Integer idjobposition = Integer.valueOf(text);
		return this.jobpositionService.findJobPositionByIdjobposition(idjobposition);
	}

}

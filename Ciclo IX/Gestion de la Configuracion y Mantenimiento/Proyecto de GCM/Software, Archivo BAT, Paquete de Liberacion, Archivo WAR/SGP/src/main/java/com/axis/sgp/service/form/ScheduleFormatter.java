package com.axis.sgp.service.form;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Service;

import com.axis.sgp.entity.Schedule;
import com.axis.sgp.service.ScheduleService;

@Service("scheduleFormatter")
public class ScheduleFormatter implements Formatter<Schedule> {

	@Autowired
	@Qualifier("scheduleServiceImpl")
	private ScheduleService scheduleService;

	@Override
	public String print(final Schedule object, final Locale locate) {
		return (object != null ? object.getIdschedule().toString() : "");
	}

	@Override
	public Schedule parse(final String text, final Locale locate) throws ParseException {
		final Integer idschedule = Integer.valueOf(text);
		return this.scheduleService.findScheduleByIdschedule(idschedule);
	}

}

package com.axis.sgp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.axis.sgp.entity.Department;
import com.axis.sgp.repository.DepartmentRepository;
import com.axis.sgp.service.DepartmentService;

@Service("departmentServiceImpl")
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	@Qualifier("departmentRepository")
	private DepartmentRepository departmentRepository;

	@Override
	public Department findDepartmentByIddepartment(Integer iddepartment) {
		return departmentRepository.findByIddepartment(iddepartment);
	}

}

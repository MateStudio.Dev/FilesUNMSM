package com.axis.sgp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.axis.sgp.entity.JobPosition;
import com.axis.sgp.repository.JobPositionRepository;
import com.axis.sgp.service.JobPositionService;

@Service("jobpositionServiceImpl")
public class JobPositionServiceImpl implements JobPositionService {

	@Autowired
	@Qualifier("jobpositionRepository")
	private JobPositionRepository jobpositionRepository;

	@Override
	public JobPosition findJobPositionByIdjobposition(Integer idjobposition) {
		return jobpositionRepository.findByIdjobposition(idjobposition);
	}

	@Override
	public List<JobPosition> listAllJobPositions() {
		return jobpositionRepository.findAll();
	}

}

package com.axis.sgp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.axis.sgp.entity.Schedule;
import com.axis.sgp.repository.ScheduleRepository;
import com.axis.sgp.service.ScheduleService;

@Service("scheduleServiceImpl")
public class ScheduleServiceImpl implements ScheduleService {

	@Autowired
	@Qualifier("scheduleRepository")
	private ScheduleRepository scheduleRepository;

	@Override
	public List<Schedule> listAllSchedules() {
		return scheduleRepository.findAll();
	}

	@Override
	public Schedule findScheduleByIdschedule(Integer idschedule) {
		return scheduleRepository.findByIdschedule(idschedule);
	}

}

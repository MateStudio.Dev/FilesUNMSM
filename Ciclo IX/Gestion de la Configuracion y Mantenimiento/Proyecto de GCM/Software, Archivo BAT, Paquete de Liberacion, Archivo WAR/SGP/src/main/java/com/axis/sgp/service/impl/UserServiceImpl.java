package com.axis.sgp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.axis.sgp.converter.UserConverter;
import com.axis.sgp.entity.User;
import com.axis.sgp.model.UserModel;
import com.axis.sgp.repository.UserRepository;
import com.axis.sgp.service.UserService;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService {

	@Autowired
	@Qualifier("userRepository")
	private UserRepository userRepository;

	@Autowired
	@Qualifier("userConverter")
	private UserConverter userConverter;

	@Override
	public UserModel addUser(UserModel userModel) {
		User user = userConverter.convertUserModel2User(userModel);
		return userConverter.convertUser2UserModel(userRepository.save(user));
	}
	
	@Override
	public UserModel updateUser(UserModel userModel) {
		User user = userConverter.convertUserModel2User(userModel);
		return userConverter.convertUser2UserModel(userRepository.save(user));
	}

	@Override
	public List<UserModel> listAllUsers() {
		List<User> users = userRepository.findAll();
		List<UserModel> usersModel = new ArrayList<UserModel>();
		for (User user : users) {
			usersModel.add(userConverter.convertUser2UserModel(user));
		}
		return usersModel;
	}

	@Override
	public UserModel findUserByDni(String dni) {
		return userConverter.convertUser2UserModel(userRepository.findByDni(dni));
	}

	@Override
	public void removeUser(String iduser) {
		User user = userConverter.convertUserModel2User(findUserByDni(iduser));
		if (user != null) {
			userRepository.delete(user);
		}
	}

	@Override
	public UserModel findUserByEmail(String email) {
		return userConverter.convertUser2UserModel(userRepository.findByEmail(email));
	}
	
	@Override
	public UserModel findUserByIduser(Integer iduser) {
		return userConverter.convertUser2UserModel(userRepository.findByIduser(iduser));
	}

}
create database Exc1yExc2EnBD1;
create table Stock (
	StockID INT AUTO_INCREMENT PRIMARY KEY,
    ProductsubcategoryID INT,
	QuantityInStock INT
);

INSERT INTO Stock (ProductsubcategoryID, QuantityInStock) VALUES (1, 23);
INSERT INTO Stock (ProductsubcategoryID, QuantityInStock) VALUES (4, 5);
INSERT INTO Stock (ProductsubcategoryID, QuantityInStock) VALUES (6, 2);
INSERT INTO Stock (ProductsubcategoryID, QuantityInStock) VALUES (15, 67);
INSERT INTO Stock (ProductsubcategoryID, QuantityInStock) VALUES (23, 12);
INSERT INTO Stock (ProductsubcategoryID, QuantityInStock) VALUES (36, 9);

select count(ProductSubcategoryID) from productoxcategoriaxsubcategoria;

select QuantityInStock from stock;

select pcs.ProductCategoryID, pcs.NameCategory, pcs.ProductSubcategoryID, pcs.Name, s.QuantityInStock from productoxcategoriaxsubcategoria pcs join stock s where (pcs.ProductSubcategoryID = s.ProductSubcategoryID AND (s.QuantityInStock is not null));



DELIMITER $$
CREATE PROCEDURE simple_loop ( ) 
BEGIN
  DECLARE counter BIGINT DEFAULT 0;
  my_loop: LOOP
    SET counter=counter+1;
    IF counter<((select count(ProductSubcategoryID) from productoxcategoriaxsubcategoria)+1) THEN
		INSERT INTO Stock (ProductsubcategoryID, QuantityInStock) VALUES (counter, 0);
      
    END IF;
    SELECT counter;
  END LOOP my_loop;
END$$
DELIMITER ;

CALL simple_loop();



select * from product;
select * from productoxcategoriaxsubcategoria;




create database proyectobi;

drop table if exists dim_tiempo;
create table dim_tiempo (
	id_tiempo VARCHAR(8), 
	anio VARCHAR(4), 
	mes VARCHAR(2), 
	dia VARCHAR(2),
    primary key (id_tiempo)
);

drop table if exists dim_sistema;
create table dim_sistema (
	id_sistema VARCHAR(8), 
	tipo_sistema VARCHAR(20),
    primary key (id_sistema)
);

drop table if exists dim_estado;
create table dim_estado (
	id_estado VARCHAR(8), 
	tipo_estado VARCHAR(20),
    primary key (id_estado)
);

drop table if exists hecho_cita;
create table hecho_cita (
    id_tiempo VARCHAR(8), 
    id_sistema VARCHAR(8), 
    id_estado VARCHAR(8), 
    PRIMARY KEY (id_tiempo, id_sistema, id_estado),
    CONSTRAINT FK_sistemacita FOREIGN KEY (id_sistema) REFERENCES dim_sistema(id_sistema),
    CONSTRAINT FK_tiempocita FOREIGN KEY (id_tiempo) REFERENCES dim_tiempo(id_tiempo),
    CONSTRAINT FK_estadocita FOREIGN KEY (id_estado) REFERENCES dim_estado(id_estado)
);

/*
alter table hecho_cita ADD CONSTRAINT FK_sistemacita FOREIGN KEY (id_sistema) REFERENCES dim_sistema(id_sistema);
alter table hecho_cita ADD CONSTRAINT FK_tiempocita FOREIGN KEY (id_tiempo) REFERENCES dim_tiempo(id_tiempo);
alter table hecho_cita ADD CONSTRAINT FK_estadocita FOREIGN KEY (id_estado) REFERENCES dim_estado(id_estado);

*/
DECLARE FechaDesde date,
DECLARE FechaHasta date;
BEGIN
FechaDesde :=TO_DATE('01/08/17','DD/MM/YY');
FechaHasta := TO_DATE((TO_CHAR(sysdate,'DD/MM/YY')),'DD/MM/YY');
WHILE FechaDesde <= FechaHasta LOOP
INSERT INTO DIM_TIEMPO
(
Id_tiempo,
Anio,
Mes,
Dia
)
VALUES
(
to_char(FechaDesde,'DD/MM/YY'),
to_char(FechaDesde,'YY'),
to_char(FechaDesde,'MM'),
to_char(FechaDesde,'DD')
);
FechaDesde := FechaDesde + 1;
END LOOP;
END;



DELIMITER $$
DROP PROCEDURE IF EXISTS `antDIM_TIEMPO`$$
CREATE PROCEDURE `antDIM_TIEMPO`()
BEGIN
DECLARE FechaDesde date;
DECLARE FechaHasta date;
delete from DIM_TIEMPO;
SELECT STR_TO_DATE('01/08/17','%d/%m/%y') INTO FechaDesde;
SELECT STR_TO_DATE(DATE_FORMAT(sysdate(),'%d/%m/%y'),'%d/%m/%y') INTO FechaHasta;
while (FechaDesde <= FechaHasta) DO
INSERT INTO DIM_TIEMPO
(
	Id_tiempo,
	Anio,
	Mes,
	Dia
)
SELECT  DATE_FORMAT(FechaDesde,'%d/%m/%y'),
		DATE_FORMAT(FechaDesde,'%y'),
		DATE_FORMAT(FechaDesde,'%m'),
		DATE_FORMAT(FechaDesde,'%d');
set FechaDesde = DATE_ADD(FechaDesde, INTERVAL 1 DAY);
END WHILE;
END$$
DELIMITER ;

select * from road_truck_transaction rt left join road_gate_appointment rg on rt.APPOINTMENT_NBR=rg.ID limit 0,100;

select * from road_truck_transaction rt;
select * from road_gate_appointment rg;


call antDIM_TIEMPO();
select * from dim_tiempo;
Fingiendo realidades 
con sombra vana, 
delante del Deseo 
va la Esperanza. 
Y sus mentiras, 
como el f�nix, renacen 
de sus cenizas.
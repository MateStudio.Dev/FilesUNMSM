Alguna vez la encuentro por el mundo, 
y pasa junto a m�; 
y pasa sonri�ndose, y yo digo: 
?�C�mo puede re�r? 

Luego asoma a mi labio otra sonrisa, 
m�scara del dolor, 
y entonces pienso: ?Acaso ella se r�e, 
como me r�o yo.
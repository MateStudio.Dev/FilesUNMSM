#http://grokbase.com/t/r/r-help-es/097hmmvj36/r-es-ayuda-con-el-paquete-de-text-mining-tm
#https://stackoverflow.com/questions/32523544/how-to-remove-error-in-term-document-matrix-in-r 
#https://enclaveder.wordpress.com/tag/rstudio/
#http://ethen8181.github.io/machine-learning/clustering_old/tf_idf/tf_idf.html
#Definimos la ruta donde se encuentran los archivos que conformaran el corpus
setwd("D:\\")
cname <- file.path("C:\\Users\\WsupernaturalW\\Desktop\\Business Intelligence\\Semana 11\\ArchivosParaCorpus1")
dir(cname)
#Se crea el corpus que contiene los 20 documentos TXT
library(tm)
corpus <- Corpus(DirSource(cname))
corpus
#Informacion general del corpus
#summary(corpus) 
#Informacion del elemento 1 del corpus
inspect(corpus[1])
##Transformaciones
# Convertimos todo en min�sculas
corpus <- tm_map(corpus, tolower)
# Quitamos los n�meros
corpus <- tm_map(corpus, removeNumbers)
# Quitamos los signos de puntuaci�n
corpus <- tm_map(corpus, removePunctuation)
# Se eliminan varias palabras comunes del ingl�s
corpus <- tm_map(corpus, removeWords, stopwords("spanish"))
# Quitamos los espacios que han salido con tanto retoque
corpus <- tm_map(corpus, stripWhitespace)
# Nos aseguramos que el corpus es texto plano
#corpus <- tm_map(corpus, PlainTextDocument)
#Visualizamos como queda el corpus tras las transformaciones
inspect(corpus[1])
#https://www.linkedin.com/pulse/3-weight-methods-term-document-matrix-r-beibei-kong
#https://www.rdocumentation.org/packages/tm/versions/0.7-1/topics/TermDocumentMatrix
#https://rstudio-pubs-static.s3.amazonaws.com/118341_dacd8e7a963745eeacf25f96da52770e.html
#https://rpubs.com/jboscomendoza/mineria-de-textos-con-r
library(SnowballC)
# Matriz de t�rminos
#corpus <- Corpus(VectorSource(corpus))
#dtm <- DocumentTermMatrix(corpus)
dtm = TermDocumentMatrix(corpus,control = list(weighting = weightTfIdf,
                                        stopwords = "spanish", 
                                        removePunctuation = T,
                                        removeNumbers = T,
                                        stemming = T))
dtm
inspect(dtm[1:200,1:10])
# make word frequency
wordFreq <- sort(rowSums(as.matrix(dtm)), decreasing = TRUE)
#https://rstudio-pubs-static.s3.amazonaws.com/132792_864e3813b0ec47cb95c7e1e2e2ad83e7.html
# make word cloud
wordcloud(words = names(wordFreq), freq = wordFreq,
          min.freq = 4, random.order = F,rot.per=0.2, scale=c(2, .1), colors = brewer.pal(6, 'Dark2'),max.words=40)
#freq <- colSums(as.matrix(dtm))
#ord <- order(freq)
# Lista terminos menos frecuentes
#freq[head(ord)]
# Lista terminos m�s frecuentes
#freq[tail(ord)]

